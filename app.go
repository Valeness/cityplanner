package main

import (
	"engo.io/engo"
	"engo.io/ecs"
	"engo.io/engo/common"
	"log"
)

type myScene struct {}

type Tile struct {
	ecs.BasicEntity
	common.RenderComponent
	common.SpaceComponent
}

// Type uniquely defines your game type
func (*myScene) Type() string { return "myGame" }

// Preload is called before loading any assets from the disk,
// to allow you to register / queue them
func (*myScene) Preload() {
	engo.Files.Load("maptiles/beach.png")
	engo.Files.Load("maptiles/grass.png")
	engo.Files.Load("maptiles/dirt.png")
	engo.Files.Load("maptiles/roadEW.png")
	engo.Files.Load("maptiles/roadNS.png")
	engo.Files.Load("maptiles/roadES.png")
	engo.Files.Load("maptiles/roadNE.png")
	engo.Files.Load("maptiles/roadNW.png")
	engo.Files.Load("maptiles/roadSW.png")
}

func CreateTile(tile_type string)(Tile) {
	tile := Tile{BasicEntity: ecs.NewBasic()}
	tile.SpaceComponent = common.SpaceComponent{
		Position: engo.Point{10, 10},
		Width:    100,
		Height:   65,
	}
	switch tile_type {
		case "beach":
			texture, err := common.LoadedSprite("maptiles/beach.png")
			if err != nil {
				log.Println("Unable to load texture: " + err.Error())
			}
			tile.RenderComponent = common.RenderComponent{
				Drawable: texture,
				Scale:    engo.Point{1, 1},
			}
		break;

		case "dirt":
			texture, err := common.LoadedSprite("maptiles/dirt.png")
			if err != nil {
				log.Println("Unable to load texture: " + err.Error())
			}
			tile.RenderComponent = common.RenderComponent{
				Drawable: texture,
				Scale:    engo.Point{1, 1},
			}
		break;
		case "roadEW":
			texture, err := common.LoadedSprite("maptiles/roadEW.png")
			if err != nil {
				log.Println("Unable to load texture: " + err.Error())
			}
			tile.RenderComponent = common.RenderComponent{
				Drawable: texture,
				Scale:    engo.Point{1, 1},
			}
		break;
		case "roadNS":
			texture, err := common.LoadedSprite("maptiles/roadNS.png")
			if err != nil {
				log.Println("Unable to load texture: " + err.Error())
			}
			tile.RenderComponent = common.RenderComponent{
				Drawable: texture,
				Scale:    engo.Point{1, 1},
			}
		break;
		case "roadES":
			texture, err := common.LoadedSprite("maptiles/roadES.png")
			if err != nil {
				log.Println("Unable to load texture: " + err.Error())
			}
			tile.RenderComponent = common.RenderComponent{
				Drawable: texture,
				Scale:    engo.Point{1, 1},
			}
		break;
		case "roadNE":
			texture, err := common.LoadedSprite("maptiles/roadNE.png")
			if err != nil {
				log.Println("Unable to load texture: " + err.Error())
			}
			tile.RenderComponent = common.RenderComponent{
				Drawable: texture,
				Scale:    engo.Point{1, 1},
			}
		break;
		case "roadNW":
			texture, err := common.LoadedSprite("maptiles/roadNW.png")
			if err != nil {
				log.Println("Unable to load texture: " + err.Error())
			}
			tile.RenderComponent = common.RenderComponent{
				Drawable: texture,
				Scale:    engo.Point{1, 1},
			}
		break;
		case "roadSW":
			texture, err := common.LoadedSprite("maptiles/roadSW.png")
			if err != nil {
				log.Println("Unable to load texture: " + err.Error())
			}
			tile.RenderComponent = common.RenderComponent{
				Drawable: texture,
				Scale:    engo.Point{1, 1},
			}
		break;
		case "grass":
			texture, err := common.LoadedSprite("maptiles/grass.png")
			if err != nil {
				log.Println("Unable to load texture: " + err.Error())
			}
			tile.RenderComponent = common.RenderComponent{
				Drawable: texture,
				Scale:    engo.Point{1, 1},
			}
		break;
	}

	return tile

}

// Setup is called before the main loop starts. It allows you
// to add entities and systems to your Scene.
func (*myScene) Setup(u engo.Updater) {
	world, _ := u.(*ecs.World)
	world.AddSystem(&common.RenderSystem{})

	tilemap := [2][4]string {
		{"grass", "roadSW", "roadNS", "roadNS"},
		{"grass", "roadEW", "grass", "dirt"},
	}

	for _, system := range world.Systems() {
		switch sys := system.(type) {
		case *common.RenderSystem:
			for rowcount, row := range tilemap {
				for cellcount, tilename := range row {
					x := float32((cellcount - rowcount) * 50)
					y := float32((cellcount + rowcount) * 25)

					log.Println(x, y);

					tile := CreateTile(tilename)
					tile.SpaceComponent.Position = engo.Point{x, y}
					sys.Add(&tile.BasicEntity, &tile.RenderComponent, &tile.SpaceComponent)
				}
			}
		}
	}
}

func main() {
	opts := engo.RunOptions{
		Title: "City Planner",
		Width:  640,
		Height: 480,
	}
	engo.Run(opts, &myScene{})
}